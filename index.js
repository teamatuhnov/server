var express = require('express');
var app = express();
var path = require('path');
var http = require('http');

var port = 3000;

app.set('port', port);

var server = http.createServer(app);
var io = require('socket.io')(server);

server.listen(port);
server.on('listening', onListening);

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
  res.sendFile(__dirname + 'public/index.html');
});

var conn = [];
var tables = [];

io.on('connection', function(socket){
  var socketId = conn.length;
  socket.inAppId = socketId;
  conn[socketId] = {
    id: socketId,
    socket: socket
  };

  console.log('a user connected to socket ' + socketId);
  addListeners(socket)
});

function onListening() {
  console.log('listening on *:' + port);
}


function addListeners(socket) {
  socket.on('action', function (action) {
    console.log('Event "action" on socket ' + this.inAppId);
    console.log(action);
  });

  socket.on('createTable', function (EventData) {
    var tableId = tables.length;
    tables[tableId] = {
      id: tableId,
      playerCount: EventData.maxPlayers ? EventData.maxPlayers : EventData.playerCount,
      playerList: [],
      game: {
        cardList: [],
      },
      tableSocket: this
    };
    conn[this.inAppId].type = 'table';
    conn[this.inAppId].tableId = tableId;

    console.log('Event "createTable" on socket ' + this.inAppId + ". New table id: "+ tableId);
    console.log(EventData);
  });

  socket.on('playerConnect', function (EventData) {
    var tableId = EventData.tableId ? EventData.tableId : 0;

    var playerId = tables[tableId].playerList.length;
    tables[tableId].playerList[playerId] = {
      id: playerId,
      tableId: tableId,
      name: EventData.name,
      cardList: [],
      socketId: this.inAppId
    };

    conn[this.inAppId].type = 'table';
    conn[this.inAppId].tableId = tableId;
    conn[this.inAppId].playerId = playerId;

    console.log('Event "playerConnect" on socket ' + this.inAppId);
    console.log(EventData);
    console.log('Added player to table ' + tableId);

    if (playerId+1 == tables[tableId].playerCount)
    {
      startGame(tableId);
    }
    else if (playerId+1 > tables[tableId].playerCount)
    {
      console.log('Error: table ' + tableId + ' is full, cant add player')
    }
  });

  socket.on('turn', function (EventData) {
    tables.forEach(function (table, id) {
      table.tableSocket.emit('turn', EventData);
    });

    return;

    var card = EventData.card;
    var pass = EventData.pass;
    var tableId = conn[this.inAppId].tableId;
    var playerId = conn[this.inAppId].playerId;

    if (card) {
      // remove card from player hand
      var cardIndex = tables[tableId].playerList[playerId].cardList.indexOf(card);
      if (cardIndex > -1) {
        tables[tableId].playerList[playerId].cardList.splice(cardIndex, 1);
      }

      // put card in game
      tables[tableId].game.cardList.push(card);
      tables[tableId].game.card = card;
      tables[tableId].playerList[playerId].active = false;

      swapAttack(tableId);
    }
    if (pass)
    {
      switchTurn(tableId);
    }
  })
}

function getActivePlayers(tableId) {
  var attackId;
  var defenseId;
  tables[tableId].playerList.forEach(function (player, id) {
    if (player.status == 'attack')
    {
      attackId = id;
    }
    if (player.status == 'defense')
    {
      defenseId = id;
    }
  });

  return {attackId: attackId, defenseId: defenseId};
}

function swapAttack(tableId) {
  var active = getActivePlayers(tableId);

  // swap
  var tmp = tables[tableId].playerList[active.defenseId].active;
  tables[tableId].playerList[active.defenseId].active = tables[tableId].playerList[active.attackId].active;
  tables[tableId].playerList[active.attackId].active = tmp;
}

function switchTurn(tableId) {
  var active = getActivePlayers(tableId);

  var nextAttacker = active.defenseId;
  var nextDefender = (nextAttacker+1) % tables[tableId].playerCount;

  // update statuses
  tables[tableId].playerList[active.attackId].status = 'waiting';
  tables[tableId].playerList[nextAttacker].status = 'attack';
  tables[tableId].playerList[nextDefender].status = 'defend';
}

function shuffle(array) {
  var i;
  var j = 0;
  var temp = null;

  for (i = array.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i + 1));
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  return array;
}

function createDeck()
{
  var deck = [];
  for (var suitId = 0; suitId <= 3; suitId++)
  {
    for (var cardRank = 4; cardRank <= 12; cardRank++)
    {
      deck[deck.length] = 100 * suitId + cardRank;
    }
  }

  return deck;
}

function startGame(tableId) {
  console.log('starting game on table ' + tableId);
  var deck = shuffle(createDeck());

  // deal cards
  console.log('Dealing cards on table ' + tableId);
  console.log(tableId);
  tables[tableId].playerList.forEach(function (player) {
    for (var i=0; i<6; i++)
    {
      var card = deck.pop();
      player.cardList.push(card);
    }
  });

  tables[tableId].deck = deck;

  // get trump card
  console.log('Choosing trump card on table ' + tableId);
  var trumpCard = deck.pop();
  deck.unshift(trumpCard);

  tables[tableId].game = {
    trumpCard: trumpCard
//    trumpSuitId = trumpCard % 100
  };

  // notify tv
  console.log('Notifying tv on table ' + tableId);
  var payloadTv = {
    'game': tables[tableId].game,
    'deck': tables[tableId].deck,
    'playerList': tables[tableId].playerList
  };
  tables[tableId].tableSocket.emit('startGame', payloadTv);
  console.log('Notify tv done on table ' + tableId);

  // notify players
  console.log('Notifying players on table ' + tableId);
  tables[tableId].playerList.forEach(function (player, id) {
    var payloadPlayer = {cardList: player.cardList};
    conn[player.socketId].socket.emit('refreshHand', payloadPlayer);
    console.log('Notify player ' + id + ' on table ' + tableId);
    console.log(payloadPlayer);
  });
}
